package org.zcs.spike.server.spark.testing.java.context.manual;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;

public class JavaSparkContextCreationTest {
    @Test
    public void testManualCreation() {
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("test")
                .set("spark.ui.enabled", "false");

        JavaSparkContext jsc = new JavaSparkContext(conf);
        // use it
        jsc.stop();
    }

}
