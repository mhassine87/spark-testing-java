==========================================
DataFrame
==========================================

Empty with predefined structure
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val df = Seq.empty[(String, Int)].toDF("color", "weight")

Empty with case class structure
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val df = Seq.empty[Apple].toDF()

Empty with struct field
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val df = spark.emptyDataFrame
          .withColumn("apple", struct(lit("green") as "color", lit(110) as "weight"))


From primitive list
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val df = List(1, 2, 3).toDF("values")

From tuples (often used)
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val df = List(("green", 70), ("red", 110)).toDF("color", "weight")


Array field
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val df = List(
          Array("red", "green", "yellow"),
          Array("green", "yellow")
        ).toDF()

With null values
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        List(null.asInstanceOf[Integer]).toDF("color")


Example: `DataFrameCreationSpec.scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/data_preparation/DataFrameCreationSpec.scala>`_