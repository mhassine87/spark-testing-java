==========================================
Data preparation
==========================================
Code examples in package: `data_preparation <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/tree/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/data_preparation>`_

.. toctree::
    :hidden:

    RDD <rdd.rst>
    DataFrame <dataframe.rst>
    Dataset <dataset.rst>
    Dynamically on fly <dynamically_modified_on_fly.rst>
