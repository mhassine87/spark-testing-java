==========================================
Framework: spark-test-jar
==========================================

Inclusion in project with Maven
"""""""""""""""""""""""""""""""

.. code-block:: java

        <dependency>
            <groupId>${spark.groupId}</groupId>
            <artifactId>spark-sql_${scala.suffix}</artifactId>
            <version>${spark.version}</version>
            <scope>provided</scope>
            <type>test-jar</type>
        </dependency>
        <dependency>
            <groupId>${spark.groupId}</groupId>
            <artifactId>spark-core_${scala.suffix}</artifactId>
            <version>${spark.version}</version>
            <scope>provided</scope>
            <type>test-jar</type>
        </dependency>
        <dependency>
            <groupId>${spark.groupId}</groupId>
            <artifactId>spark-catalyst_${scala.suffix}</artifactId>
            <version>${spark.version}</version>
            <scope>provided</scope>
            <type>test-jar</type>
        </dependency>

Example of `pom.xml <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/pom.xml>`_

Enabling in testcases
"""""""""""""""""""""""""""""""""""""

Two traits have to be included for enable resources "QueryTest", "SharedSQLContext".

.. code-block:: scala

            class SparkTestJarSpec extends QueryTest with SharedSQLContext with Matchers


"SparkSession" and "SparkContext"
"""""""""""""""""""""""""""""""""""""
Can be received with methods: "spark" "sparkContext".

.. code-block:: scala
        :emphasize-lines: 3

        val weights = List(120, 150)
        val expected = weights.reduce((a, v) => a + v)
        val df = spark.createDataset(weights).toDF("weight")


Code example: `SparkTestJarSpec.scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/context/SparkTestJarSpec.scala>`_